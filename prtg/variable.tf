
variable "vpc_cidr" {
  default = "10.0.0.0/16"
}

variable "pri3_cidr" {
  default = ["10.0.3.0/24", "10.0.1.0/24", "10.0.2.0/24"]
}


variable "sub_cidr" {
  default = "10.0.5.0/24"
}

variable "http-http" {
  default = ["80", "443"]
}
variable "all_icmp-cidr_access" {
  type    = list(any)
  default = ["74.205.243.161/32", "65.248.81.0/24", "204.90.98.0/24"]
}

variable "instance_type" {
  default = "t2.micro"
}

variable "ssh_access" {
  default = ["67.158.51.0/24", "204.90.98.0/24"]
}

data "aws_ami" "prtg_ami" {
  most_recent = true
  owners      = ["amazon"]

  filter {
    name   = "name"
    values = ["Windows_Server-2022-English-Full-Base-2022.10.27"]
  }

  filter {
    name   = "root-device-type"
    values = ["ebs"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }
}

locals {
  database_cidr = {
    dbase1 = {
      cidr_block = "10.0.6.0/24"
      az         = "us-east-1f"
    }

    dbase2 = {
      cidr_block = "10.0.7.0/24"
      az         = "us-east-1b"
    }

    dbase3 = {
      cidr_block = "10.0.8.0/24"
      az         = "us-east-1c"
    }
  }

  linus_vm = {
    ami           = data.aws_ami.my_golden_image.id
    subnet_id     = aws_subnet.database["dbase2"].id
    instance_type = var.instance_type
  }

  database_instances = {
  wid_vm = {
    ami           = data.aws_ami.prtg_ami.id
    subnet_id     = aws_subnet.database["dbase1"].id
    instance_type = var.instance_type
  }
}
}

data "aws_ami" "my_golden_image" {
  most_recent = true
  owners      = ["amazon"]

  filter {
    name   = "name"
    values = ["amzn2-ami-kernel-5.10-hvm-*-gp2"]
  }

  filter {
    name   = "root-device-type"
    values = ["ebs"]
  }
}