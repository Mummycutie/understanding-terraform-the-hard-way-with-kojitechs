#This is the vpc-network of prtg
resource "aws_vpc" "prtg_vpc" {
  cidr_block       = var.vpc_cidr
  instance_tenancy = "default"

  tags = {
    Name = "prtg_vpc"
  }
}

#Give internet access to the network
resource "aws_internet_gateway" "gw" {
  vpc_id = aws_vpc.prtg_vpc.id

  tags = {
    Name = "igw"
  }
}
#Creating subnet with access to internet
resource "aws_subnet" "prtg_sub" {
  vpc_id            = aws_vpc.prtg_vpc.id
  cidr_block        = var.sub_cidr
  availability_zone = "us-east-1a"
  map_public_ip_on_launch = false

  tags = {
    Name = "prtg_sub"
  }
}

resource "aws_route_table" "pub_sub_route_table" {
  vpc_id = aws_vpc.prtg_vpc.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.gw.id
  }

  tags = {
    Name = "sub_route_table"
  }
}

resource "aws_route_table_association" "sub_assoc" {
  subnet_id      = aws_subnet.prtg_sub.id
  route_table_id = aws_route_table.pub_sub_route_table.id
}

#private subnet dynamically

resource "aws_subnet" "private" {
  count             = length(var.pri3_cidr)
  vpc_id            = aws_vpc.prtg_vpc.id
  availability_zone = "us-east-1f"
  cidr_block        = var.pri3_cidr[count.index]
}

resource "aws_subnet" "database" {
  for_each          = local.database_cidr
  vpc_id            = aws_vpc.prtg_vpc.id
  cidr_block        = each.value.cidr_block
  availability_zone = each.value.az

  tags = {
    "name" = "each.key"
  }
}

resource "aws_instance" "probe" {
  ami                     = data.aws_ami.prtg_ami.id
  instance_type           = var.instance_type
  subnet_id               = aws_subnet.prtg_sub.id
  vpc_security_group_ids  = [aws_security_group.prtg_sg.id]
  key_name                = aws_key_pair.prtg-key.key_name

  tags = {
    Name = "prtg_probe"
  }
}

# resource "aws_eip" "elastic_ip" {
#   instance = aws_instance.probe.id
#   vpc      = true
# }

# resource "aws_eip_association" "eip_assoc" {
#   instance_id   = aws_instance.probe.id
#   allocation_id = aws_eip.elastic_ip.id
# }

resource "aws_key_pair" "prtg-key" {
  key_name   = "prtg_key"
  public_key = tls_private_key.rsa.public_key_openssh
}

# RSA key of size 4096 bits
resource "tls_private_key" "rsa" {
  algorithm = "RSA"
  rsa_bits  = 4096
}

resource "local_file" "private_pem" {
  content  = tls_private_key.rsa.private_key_pem
  filename = "pem_key"
}

resource "aws_security_group" "prtg_sg" {
  name        = "prtg_sg"
  description = "used for RDP/PRTG Probe communication"
  vpc_id      = aws_vpc.prtg_vpc.id

  ingress {
    description = "All ICMP-IPv4 access"
    from_port   = -1
    to_port     = -1
    protocol    = "icmp"
    cidr_blocks = var.all_icmp-cidr_access
  }

  ingress {
    description = "RDP access"
    from_port   = 3389
    to_port     = 3389
    protocol    = "tcp"
    cidr_blocks = ["204.90.98.254/32"]
  }

  # ingress {
  #   description = "Custom TCP access"
  #   from_port   = 23560
  #   to_port     = 23560
  #   protocol    = "tcp"
  #   cidr_blocks = ["204.90.98.168/32"]
  # }

  # egress {
  #   from_port   = 8443
  #   to_port     = 8443
  #   protocol    = "tcp"
  #   cidr_blocks = ["67.158.51.0/24"]
  # }

  # egress {
  #   from_port   = var.https_http[0]
  #   to_port     = var.https_http[1]
  #   protocol    = "tcp"
  #   cidr_blocks = ["0.0.0.0/0"]
  # }

  # egress {
  #   from_port   = -1
  #   to_port     = -1
  #   protocol    = "icmp"
  #   cidr_blocks = ["74.205.243.161/32"]
  # }

  # egress {
  #   from_port   = 22
  #   to_port     = 22
  #   protocol    = "tcp"
  #   cidr_blocks = var.ssh_access
  # }

  # egress {
  #   from_port   = 0
  #   to_port     = 65535
  #   protocol    = "tcp"
  #   cidr_blocks = ["35.163.181.119/32"]
  # }
  # egress {
  #   from_port   = 23560
  #   to_port     = 23560
  #   protocol    = "tcp"
  #   cidr_blocks = ["204.90.98.168/32"]
  # }

  # egress {
  #   from_port   = 25
  #   to_port     = 25
  #   protocol    = "tcp"
  #   cidr_blocks = ["204.90.98.160/32"]
  # }

  tags = {
    "name" = "prtg_sec.grp"
  }
}

# private instance dynamically
resource "aws_instance" "database_instances" {
  for_each      = local.database_instances
  ami           = each.value.ami
  subnet_id     = each.value.subnet_id
  instance_type = each.value.instance_type

  tags = {
    name = "each.key"
  }

}