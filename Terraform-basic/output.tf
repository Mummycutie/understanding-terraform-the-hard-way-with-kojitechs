
##OUTPUT BLOCK: used to display or print on terminal
output "private_dns" {
  description = "dns of the ecs"
  value = aws_instance.test1.private_dns
}

output "priv_id" {
  value = aws_subnet.priv[0].id
  description = "subnet id of all priv. subnet"
}

