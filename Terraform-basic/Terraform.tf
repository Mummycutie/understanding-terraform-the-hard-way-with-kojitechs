
#Terraform block is meant to set constraint on terraform version

terraform {
    required_version = ">=1.1.0"

  required_providers {
    aws = {
      source = "hashicorp/aws"
      version = "~>4.0"
    }
  }
}

#Provider Block : This is the AWS crediential for authenticate user
provider "aws" {
  region = "us-east-1"
  profile = "default"
}

## Resource Block (Creating resources/bring into existence)

resource "aws_vpc" "connectivity" {
  cidr_block       = var.vpc_cidr
  instance_tenancy = "default"

  tags = {
    Name = "connectivity"
  }
}

##subnet
# resource "aws_subnet" "priv" {
#   count = length(var.priv_cidr)
#   vpc_id     = local.vpc_id
#   cidr_block = var.priv_cidr[count.index]
#   availability_zone = data.aws_availability_zones.available.names[0]

# }
 
 resource "aws_subnet" "priv" {
  count = length(var.priv_cidr)
  vpc_id     = local.vpc_id
  cidr_block = var.priv_cidr[count.index]
  availability_zone = element(slice(data.aws_availability_zones.available.names, 0, 2))[count.index]

}

# resource "aws_subnet" "priv2" {
#   vpc_id     = local.vpc_id
#   cidr_block = var.priv2_cidr
#   availability_zone = data.aws_availability_zones.available.names[1]
# }

# resource "aws_subnet" "priv3" {
#   vpc_id     = local.vpc_id
#   cidr_block = var.priv3_cidr
#   availability_zone = data.aws_availability_zones.available.names[2]

# }


#Creating ec2. where: aws_instance = resource_name(Cant be changed) foo = local_name (can be name anything)
resource "aws_instance" "test1" {
  ami           = data.aws_ami.my_golden_image.id
  instance_type = "t2.micro"

  tags = {
    Name = "test1"
  }

}