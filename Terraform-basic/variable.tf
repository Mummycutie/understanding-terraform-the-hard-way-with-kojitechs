
###variable Block to avoid hardcoding sensitive info, to allow editing

variable "vpc_cidr" {
  type = string
  default = "10.0.0.0/16"
}

variable "priv_cidr" {
  type = list(any)
  default = ["10.0.1.0/24", "10.0.2.0/24", "10.0.3.0/24", "10.0.15.0/24"] 
}

# variable "priv2_cidr" {
#   type = string
#   default = "10.0.2.0/24"
# }

# variable "priv3_cidr" {
#   type = string
#   default = "10.0.3.0/24"
# }
