
locals {
  vpc_id = "aws_vpc.vpc1.id"
  azs    = slice(data.aws_availability_zones.available.names, 0, 2)
}