
variable "component" {
  type        = string
  description = "name of the project"
  default     = "jakuti_3-tier-architecture"
}

variable "vpc_cidr" {
  type = string
}

variable "public_snet_cidr" {
  type = list(any)
}

variable "private_snet_cidr" {
  type = list(any)
}

variable "database_snet_cidr" {
  type = list(any)
}