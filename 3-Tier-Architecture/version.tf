
#####################################
#configure backend
#############################

terraform {
  required_version = ">=1.1.0"
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.0"
    }
  }

  backend "s3" {
    bucket         = "3-tier-jakuti"
    key            = "path/env"
    region         = "us-east-1"
    dynamodb_table = "terraform_state_lock"
    encrypt        = true
  }
}

provider "aws" {
  region = "us-east-1"

default_tags {
  tags = {
changeCode = "100200300JAKUTI"
component = var.component
  }
}

}