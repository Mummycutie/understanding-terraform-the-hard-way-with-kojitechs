
terraform {
    required_version = ">=1.1.0"
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.0"
    }
  }

  backend "s3" {
    bucket = "tf-journey-jakuti"
    key    = "path/env"
    region = "us-east-1"
    dynamodb_table = "terraform_state_lock"
    encrypt = true
  }
}


# Configure the AWS Provider
provider "aws" {
  region = "us-east-1"
}
