
# Create a VPC
resource "aws_vpc" "this" {
  cidr_block = "10.0.0.0/16"
}

#two public subnet, 2 private subnet, 2 database subnet (using count)

resource "aws_subnet" "public_sub" {
  vpc_id     = aws_vpc.this.id
  count = length(var.cidr_block)
  cidr_block = var.cidr_block[count.index]
  availability_zone = [data.aws_availability_zones.available.names[0], data.aws_availability_zones.available.names[1]][count.index]
  map_public_ip_on_launch = true

  tags = {
    Name = "public_subnet_${count.index + 1}"
  }
}

resource "aws_subnet" "pri_sub" {
  vpc_id     = aws_vpc.this.id
  count = length(var.private_cidr_block)
  cidr_block = var.private_cidr_block[count.index]
  availability_zone =  [data.aws_availability_zones.available.names[0], data.aws_availability_zones.available.names[1]] [count.index]

  tags = {
    Name = "private_subnet_${count.index + 1}"

  }
}
