
resource "aws_vpc" "vpc1" {
  cidr_block           = var.vpc_cidr
  enable_dns_hostnames = true
  enable_dns_support   = true

  tags = {
    Name = "jakuti_vpc"
  }
}

#########################
#Create internet gateway
########################

resource "aws_internet_gateway" "gw" {
  vpc_id = local.vpc_id

  tags = {
    Name = "jakuti_igw"
  }
}

############################
#Public subnet
##########################
resource "aws_subnet" "public_snet" {
  count = length(var.public_snet_cidr)

  vpc_id                  = local.vpc_id
  cidr_block              = var.public_snet_cidr[count.index]
  availability_zone       = local.azs[count.index]
  map_public_ip_on_launch = true

  tags = {
    Name = "jakuti_pubic_snet-${count.index + 1}"
  }

}

############################
#Private subnet
##########################
resource "aws_subnet" "private_snet" {
  count = length(var.private_snet_cidr)

  vpc_id            = local.vpc_id
  cidr_block        = var.private_snet_cidr[count.index]
  availability_zone = local.azs[count.index]

  tags = {
    Name = "jakuti_private_snet-${count.index + 1}"
  }
}

############################
#database subnet
##########################
resource "aws_subnet" "database_snet" {
  count = length(var.database_snet_cidr)

  vpc_id            = local.vpc_id
  cidr_block        = var.database_snet_cidr[count.index]
  availability_zone = local.azs[count.index]

  tags = {
    Name = "jakuti_database_snet-${count.index + 1}"
  }
}

########################################
# Using for-each loop to create multiple public subnets
####################################

# resource "aws_subnet" "public_snet" {
#  for-each = var.public_snet_cidr

#   vpc_id            = local.vpc_id
#   cidr_block        = each.value.public_snet_cidr
#   availability_zone = local.azs

#   tags = {
#     Name = each.key
#   }

# }


#################################
#Creating route-table
###########################
resource "aws_route_table" "internet_route" {
  vpc_id = local.vpc_id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.gw.id
  }

  tags = {
    "Name" = "jakuti_public_rt"
  }
}

#############################
#Route Table association
#############################
resource "aws_route_table_association" "public_rt_assoc" {
  count          = length(aws_subnet.public_snet)
  subnet_id      = aws_subnet.public_snet[count.index].id
  route_table_id = aws_route_table.internet_route.id
}

###########################################
#default route-table
#######################################
resource "aws_default_route_table" "default-routetable" {
  default_route_table_id = aws_vpc.vpc1.default_route_table_id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_nat_gateway.nat.id
  }
}

#############################################
#Creating NAT gateway
#####################################
resource "aws_nat_gateway" "nat" {
  depends_on    = [aws_internet_gateway.gw]
  allocation_id = aws_eip.eip.id
  subnet_id     = aws_subnet.public_snet[0].id


  tags = {
    Name = "gw NAT"
  }
}
#########################
#create eip
#####################
resource "aws_eip" "eip" {
  depends_on = [aws_internet_gateway.gw]
  vpc        = true
}