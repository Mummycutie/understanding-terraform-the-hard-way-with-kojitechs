
#########################################
#creating albasg with resource
#########################

resource "aws_autoscaling_group" "asg" {
  name                      = "${var.component}-app1-asg"
  desired_capacity          = 2
  max_size                  = 10
  min_size                  = 2
  health_check_grace_period = 300
  health_check_type         = "EC2"
  vpc_zone_identifier       = aws_subnet.private_snet[*].id
  target_group_arns         = [aws_lb_target_group.app1.id]

  launch_template {
    id      = aws_launch_template.ec2-temp.id
    version = aws_launch_template.ec2-temp.latest_version
  }

  initial_lifecycle_hook {
    name                 = "ExampleStartupLifeCycleHook"
    default_result       = "CONTINUE"
    heartbeat_timeout    = 60
    lifecycle_transition = "autoscaling:EC2_INSTANCE_LAUNCHING"

    notification_metadata = jsonencode(
      {
        hello : "World"
      }
    )
  }

  initial_lifecycle_hook {
    name                 = "ExampleTerminationLifeCycleHook"
    default_result       = "CONTINUE"
    heartbeat_timeout    = 200
    lifecycle_transition = "autoscaling:EC2_INSTANCE_TERMINATING"

    notification_metadata = jsonencode(
      {
        hello : "World"
      }
    )
  }

  instance_refresh {
    strategy = "Rolling"
    preferences {
      min_healthy_percentage = 50
    }
    triggers = ["tag", "desired_capacity", "max_size"]

  }
}

######################
#Creating launch template
########################
resource "aws_launch_template" "ec2-temp" {
  name                   = "${var.component}-app-temp"
  description            = "This is a template for the application app1"
  vpc_security_group_ids = [aws_security_group.app_static_sg.id]
  image_id               = data.aws_ami.my_golden_image.id
  instance_type          = "t3.micro"
  user_data              = filebase64("${path.module}/templates/app1.sh")


  iam_instance_profile {
    name = aws_iam_instance_profile.instance_profile.name
  }

  block_device_mappings {
    device_name = "/dev/sda1"
    ebs {
      volume_size           = 20
      delete_on_termination = true
      volume_type           = "gp2"
    }
  }

  credit_specification {
    cpu_credits = "standard"
  }

  ebs_optimized = true
  instance_market_options {
    market_type = "spot"
  }

  monitoring {
    enabled = true
  }

  tag_specifications {
    resource_type = "instance"
    tags = {
      Name = "${var.component}-app1-launch-template"
    }
  }

}

############################################
#CReating auto-scaling policy
##################################

resource "aws_autoscaling_policy" "avg-cpu-greater-xxx" {
  autoscaling_group_name    = aws_autoscaling_group.asg.id
  name                      = "${var.component}-avg-cpu-greater-xxx"
  policy_type               = "TargetTrackingScaling"
  estimated_instance_warmup = 180

  target_tracking_configuration {
    predefined_metric_specification {
      predefined_metric_type = "ASGAverageCPUUtilization"
    }

    target_value = 50.0
  }
}

resource "aws_autoscaling_policy" "avg-cpu-greater-yyy" {
  autoscaling_group_name    = aws_autoscaling_group.asg.id
  name                      = "${var.component}-avg-cpu-greater-yyy"
  policy_type               = "TargetTrackingScaling"
  estimated_instance_warmup = 180

  target_tracking_configuration {
    predefined_metric_specification {
      predefined_metric_type = "ALBRequestCountPerTarget"
      resource_label         = "${aws_lb.alb.arn_suffix}/${aws_lb_target_group.app1.arn_suffix}"
    }
    target_value = 10.0
  }
}

#######################################
# CREATING ASG NOTIFICATION
########################################

resource "random_pet" "this" {
}

resource "aws_sns_topic" "asg-topic" {
  name = "${var.component}-${random_pet.this.id}"

}

#SNS Subscription#################
resource "aws_sns_topic_subscription" "asg-subscription" {
  topic_arn = aws_sns_topic.asg-topic.arn
  protocol  = "email"
  endpoint  = "evermore2930@gmail.com"
}

###########Autoscaling notification############
resource "aws_autoscaling_notification" "asg_notifications" {
  group_names = [aws_autoscaling_group.asg.name]

  notifications = [
    "autoscaling:EC2_INSTANCE_LAUNCH",
    "autoscaling:EC2_INSTANCE_TERMINATE",
    "autoscaling:EC2_INSTANCE_LAUNCH_ERROR",
    "autoscaling:EC2_INSTANCE_TERMINATE_ERROR",
  ]
 
  topic_arn = aws_sns_topic.asg-topic.arn
}

resource "aws_autoscaling_schedule" "asg-schedule" {
  scheduled_action_name  = "${var.component}-asg-schedule"
  min_size               = 2
  max_size               = 10
  desired_capacity       = 2
  start_time             = "2030-12-11T21:00:00Z"
  end_time               = "2030-12-12T05:00:00Z"
  autoscaling_group_name = aws_autoscaling_group.asg.name
}

#######################################
# CREATING ASG NOTIFICATION
########################################
resource "aws_autoscaling_policy" "high-cpu" {
  name                   = "foobar3-terraform-test"
  scaling_adjustment     = 4
  adjustment_type        = "ChangeInCapacity"
  cooldown               = 300
  autoscaling_group_name = aws_autoscaling_group.asg.name
}

resource "aws_cloudwatch_metric_alarm" "cloudalert" {
  alarm_name          = "${var.component}-alarm-me"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = "2"
  metric_name         = "CPUUtilization"
  namespace           = "AWS/EC2"
  period              = "120"
  statistic           = "Average"
  threshold           = "80"

  dimensions = {
    AutoScalingGroupName = aws_autoscaling_group.asg.name
  }

  alarm_description = "This metric monitors ec2 cpu utilization"
  alarm_actions     = [aws_autoscaling_policy.high-cpu.arn, aws_sns_topic.asg-topic.arn]
}