
##################################
#creating loadbalancer security group
###############################
resource "aws_security_group" "alb_sg" {
  name        = "${var.component}_alb_sg"
  description = "Allow access on http/https from everywhere"
  vpc_id      = local.vpc_id

  tags = {
    Name = "${var.component}- alb_sg"
  }
}
resource "aws_security_group_rule" "ingress_on_http" {
  security_group_id = aws_security_group.alb_sg.id
  type              = "ingress"
  from_port         = 80
  to_port           = 80
  protocol          = "tcp"
  cidr_blocks       = ["0.0.0.0/0"]
}

resource "aws_security_group_rule" "ingress_on_https" {
  security_group_id = aws_security_group.alb_sg.id
  type              = "ingress"
  from_port         = 443
  to_port           = 443
  protocol          = "tcp"
  cidr_blocks       = ["0.0.0.0/0"]
}

resource "aws_security_group_rule" "egress_access_from_everywhere" {
  security_group_id = aws_security_group.alb_sg.id
  type              = "egress"
  to_port           = 0
  protocol          = "-1"
  from_port         = 0
  cidr_blocks       = ["0.0.0.0/0"]
}

#############################
#creating app1 + app2 security group
###############################

resource "aws_security_group" "app_static_sg" {
  name        = "${var.component}_app_static_sg"
  description = "Allow alb on port 80"
  vpc_id      = local.vpc_id

  tags = {
    Name = "${var.component}- app_static_sg"
  }
}

resource "aws_security_group_rule" "app_ingress" {
  security_group_id        = aws_security_group.app_static_sg.id
  type                     = "ingress"
  from_port                = 80
  to_port                  = 80
  protocol                 = "tcp"
  source_security_group_id = aws_security_group.alb_sg.id #this replaces cidr block since it is taking from a resource sg
}

resource "aws_security_group_rule" "app_egress" {
  security_group_id = aws_security_group.app_static_sg.id
  type              = "egress"
  from_port         = 0
  to_port           = 0
  protocol          = "-1"
  cidr_blocks       = ["0.0.0.0/0"]
}
