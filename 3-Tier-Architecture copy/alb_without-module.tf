#########################################
#creating alb with resource
#########################

resource "aws_lb" "alb" {
  name               = "web-alb"
  internal           = false
  load_balancer_type = "application"
  security_groups    = [aws_security_group.alb_sg.id]
  subnets            = aws_subnet.public_snet[*].id

  enable_deletion_protection = false

  tags = {
    Name = "${var.component}-web_alb"
  }
}

###Creating target group####
resource "aws_lb_target_group" "app1" {
  name                 = "app1-tg"
  port                 = 80
  protocol             = "HTTP"
  target_type          = "instance"
  vpc_id               = local.vpc_id
  deregistration_delay = 10
  protocol_version     = "HTTP1"

  health_check {
    enabled             = true
    interval            = 30
    path                = "/app1/index.html"
    port                = "traffic-port"
    healthy_threshold   = 3
    unhealthy_threshold = 3
    timeout             = 6
    matcher             = "200-399"
  }

  tags = {
    Name = "${var.component}-app1-tg"
  }
}


######################################
#Creating a Listerners
#########################################
resource "aws_lb_listener" "http_listerner" {
  load_balancer_arn = aws_lb.alb.arn
  port              = "80"
  protocol          = "HTTP"

  default_action {
    type = "redirect"

    redirect {
      host        = "#{host}"
      path        = "/#{path}"
      port        = "443"
      protocol    = "HTTPS"
      query       = "#{query}"
      status_code = "HTTP_301"
    }
  }
}

resource "aws_lb_listener" "https_listerner" {
  load_balancer_arn = aws_lb.alb.arn
  port              = "443"
  protocol          = "HTTPS"
  ssl_policy        = "ELBSecurityPolicy-2016-08"
  certificate_arn   = aws_acm_certificate.cert.arn

  default_action {
    type = "fixed-response"

    fixed_response {
      content_type = "text/plain"
      message_body = "Fixed response content"
      status_code  = "200"
    }
  }
}

############################################
#Creating listerners rule
########################################
resource "aws_lb_listener_rule" "https-appi_rule" {
  listener_arn = aws_lb_listener.https_listerner.arn
  priority     = 1

  action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.app1.arn
  }

  condition {
    path_pattern {
      values = ["/*"]
    }
  }
}