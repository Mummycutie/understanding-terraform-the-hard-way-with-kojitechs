
vpc_cidr = "120.0.0.0/16"

public_snet_cidr          = ["120.0.1.0/24", "120.0.3.0/24"]
private_snet_cidr         = ["120.0.0.0/24", "120.0.2.0/24"]
database_snet_cidr        = ["120.0.51.0/24", "120.0.53.0/24"]
instance_type             = "t2.micro"
instance_class            = "db.t2.micro"
username                  = "janetech"
subject_alternative_names = ["*.kutsomar.click"]
domain_name               = "kutsomar.click"

# public_snet_cidr = {
#   public1 = {
#   cidr_block = "120.0.1.0/24"
#   availability zone = local.azs[0]
#     }   
#   
#   public2 = {
#    cidr_block = "120.0.3.0/24"
#   availability zone = local.azs[1]
#     }
# }