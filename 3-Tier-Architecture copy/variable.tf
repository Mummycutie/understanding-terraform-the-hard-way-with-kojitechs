
variable "component" {
  type        = string
  description = "name of the project"
  default     = "jakuti"
}

variable "vpc_cidr" {
  type = string
}

variable "public_snet_cidr" {
  type = list(any)
}

variable "private_snet_cidr" {
  type = list(any)
}

variable "database_snet_cidr" {
  type = list(any)
}

variable "instance_type" {
  type = string

}

################
#database variable
###################
variable "instance_class" {
  type = string
}

variable "username" {
}

variable "db_name" {
  type    = string
  default = "webappdb"
}

variable "port" {
  default = 3306
}

variable "domain_name" {
  type        = string
  description = "this is domain name of our route-53"
}

variable "subject_alternative_names" {
  type = list(any)
}