
data "aws_availability_zones" "available" {
  state = "available"
}

data "aws_ami" "my_golden_image" {
  most_recent = true
  owners      = ["amazon"]

  filter {
    name   = "name"
    values = ["amzn2-ami-kernel-5.10-hvm-*-gp2"]
  }

  filter {
    name   = "root-device-type"
    values = ["ebs"]
  }
}

#pull down secret manager using datasource
