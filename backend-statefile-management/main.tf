
##########################################
#Creating s3 for statefile
#########################################

resource "aws_s3_bucket" "s3-statefile" {
  bucket = var.s3_buckets_statefile

  lifecycle {
    prevent_destroy = true
  }
}

##########################################################
#locking s3 statefile
##########################################################

resource "aws_dynamodb_table" "basic-dynamodb-table" {
  name           = "terraform_state_lock"
  read_capacity  = 20
  write_capacity = 20
  hash_key       = "LockID"
  

  attribute {
    name = "LockID"
    type = "S"
  }

  lifecycle {
    prevent_destroy = true
  }
}
