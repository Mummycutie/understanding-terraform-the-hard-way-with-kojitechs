
locals {
  azs = data.aws_availability_zones.available.names

  public_subnet = {
    Public_1a = {
      cidr_block = "10.0.30.0/24"
      az         = local.azs[0]
    }

    public_1b = {
      cidr_block = "10.0.1.0/24"
      az         = local.azs[1]
    }

  }

  private_subnet = {
    private_1 = {
      cidr_block = "10.0.2.0/24"
      az         = local.azs[1]
    }

    private_2 = {
      cidr_block = "10.0.4.0/24"
      az         = local.azs[1]
    }
  }

  dbase_subnet = {
    dbase_1 = {
      cidr_block = "10.0.10.0/24"
      az         = local.azs[0]
    }
    dbase_2 = {
      cidr_block = "10.0.20.0/24"
      az         = local.azs[1]
    }
  }

  ec2_app = {
    app_1 = {
      instance_type = "t2.micro"
    }
    app_2 = {
      instance_type = "t3.micro"
    }
  }
}

data "aws_availability_zones" "available" {
  state = "available"
}

data "aws_ami" "my_golden_image" {
  most_recent = true
  owners      = ["amazon"]

  filter {
    name   = "name"
    values = ["amzn2-ami-kernel-5.10-hvm-*-gp2"]
  }

  filter {
    name   = "root-device-type"
    values = ["ebs"]
  }
}
