

##################################################################
# Create a VPC
###################################################################
resource "aws_vpc" "this" {
  cidr_block = "10.0.0.0/16"
}

#two public subnet, 2 private subnet, 2 database subnet (using count)

resource "aws_subnet" "public_subnet" {
  vpc_id                  = aws_vpc.this.id
  for_each                = local.public_subnet
  cidr_block              = each.value.cidr_block
  availability_zone       = each.value.az
  map_public_ip_on_launch = true

  tags = {
    Name = each.key
  }
}

#2 priv-subnet in 2 azs
resource "aws_subnet" "private_subnet" {
  vpc_id                  = aws_vpc.this.id
  for_each                = local.private_subnet
  cidr_block              = each.value.cidr_block
  availability_zone       = each.value.az
  map_public_ip_on_launch = false

  tags = {
    Name = each.key
  }
}

#2 database-subnet in 2 azs
resource "aws_subnet" "dbase_subnet" {
  vpc_id                  = aws_vpc.this.id
  for_each                = local.dbase_subnet
  cidr_block              = each.value.cidr_block
  availability_zone       = each.value.az
  map_public_ip_on_launch = false

  tags = {
    Name = each.key
  }
}
##########################################################################
#OUTPUTING A SPECIFIC SUBNET_ID
##########################################################################
output "public_subnet_id" {
  value = aws_subnet.public_subnet["public_1b"].id
}

output "private_subnet_id" {
  value = aws_subnet.private_subnet["private_1"].id
}

output "dbase_subnet_id" {
  value = aws_subnet.dbase_subnet["dbase_1"].id
}


##########################################################################
#OUTPUT A ALL SUBNET_ID
##########################################################################
output "all_public_subnet_ids" {
  value = [for each_subnet in aws_subnet.public_subnet: each_subnet.id]
}

output "all_private_subnet_ids" {
  value = [for each_subnet in aws_subnet.private_subnet: each_subnet.id]
}

output "all_dbase_subnet_ids" {
  value = [for each_subnet in aws_subnet.dbase_subnet: each_subnet.id]
}

#creating two ec2 in public_sub
resource "aws_instance" "ec2_app" {
  ami           = data.aws_ami.my_golden_image.id
  for_each      = local.ec2_app
  instance_type = each.value.instance_type
  subnet_id     = aws_subnet.public_subnet["public_1b"].id

  tags = {
    Name = each.key
  }
}