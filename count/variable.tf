
variable "cidr_block" {
  type = list
  description = "public subnet cidrs"
  default = ["10.0.0.0/24", "10.0.2.0/24"]
}

variable "private_cidr_block" {
  type = list
  description = "private subnet cidrs"
  default = ["10.0.3.0/24", "10.0.5.0/24"]
}


data "aws_availability_zones" "available" {
  state = "available"
}

output "azs" {
  value = data.aws_availability_zones.available.names[0]
}
